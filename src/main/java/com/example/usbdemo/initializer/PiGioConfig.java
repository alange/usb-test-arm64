package com.example.usbdemo.initializer;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalOutputConfigBuilder;
import com.pi4j.io.gpio.digital.DigitalState;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class PiGioConfig {

    private Context pi4j;

    private boolean init = false;

    private DigitalOutput doorAccess;
    private DigitalOutput doorwayIn;
    private DigitalOutput doorwayOut;



    private void init() {
        if (init) {
            return;
        }

        init = true;

        this.pi4j = Pi4J.newAutoContext();

        DigitalOutputConfigBuilder config = DigitalOutput.newConfigBuilder(pi4j)
                .id("doorAccess")
                .name("doorAccess")
                .address(27)
                .shutdown(DigitalState.LOW)
                .initial(DigitalState.LOW)
                .provider("pigpio-digital-output");

        doorAccess = pi4j.create(config.build());
        doorwayIn = pi4j.create(config.address(22).id("doorwayIn").name("doorwayIn").build());
        doorwayOut = pi4j.create(config.address(23).id("doorwayOut").name("doorwayOut").build());


        log.info(pi4j.platforms().describe().toString());
        log.info("Pi GPIO initialized");

    }

    @PreDestroy
    protected void destroy() {
        pi4j.shutdown();
    }

    public DigitalOutput getDoorAccess() {
        init();
        return doorAccess;
    }

    public DigitalOutput getDoorwayIn() {
        init();
        return doorwayIn;
    }

    public DigitalOutput getDoorwayOut() {
        init();
        return doorwayOut;
    }
}
