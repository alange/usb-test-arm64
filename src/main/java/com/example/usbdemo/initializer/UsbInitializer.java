package com.example.usbdemo.initializer;

import com.example.usbdemo.util.RandomValueGenerator;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.usb4java.*;

@Component
@Slf4j
public class UsbInitializer {

    private final Context context = new Context();

    private Device device;

    @Bean
    public void scanDevices() {
        log.info("UsbInitializer");

        for (int i = 0; i < 10; i++) {
            String generated = new RandomValueGenerator().generate();
            log.info("Generated: " + generated);
        }

        int result = LibUsb.init(context);
        if (result != LibUsb.SUCCESS) throw new LibUsbException("Unable to initialize libusb.", result);


        // Read the USB device list
        DeviceList list = new DeviceList();
        int resultList = LibUsb.getDeviceList(context, list);
        if (resultList < 0) throw new LibUsbException("Unable to get device list", result);

        try {
            // Iterate over all devices and scan for the right one
            for (Device device : list) {
                DeviceDescriptor descriptor = new DeviceDescriptor();
                result = LibUsb.getDeviceDescriptor(device, descriptor);
                if (result != LibUsb.SUCCESS) throw new LibUsbException("Unable to read device descriptor", result);
                //log.info("Found device: " + device.toString()+ "\n  descriptor:\n" + descriptor);

                String vendorId = Integer.toHexString(descriptor.idVendor() & 0xffff).toUpperCase();
                String productId = Integer.toHexString(descriptor.idProduct() & 0xffff).toUpperCase();

                log.info("Vendor ID: " + vendorId + ", Product ID: " + productId);

                if (vendorId.equals("AC90") && productId.equals("3003")) {
                    this.device = device;
                }

            }
        } finally {
            // Ensure the allocated device list is freed
            LibUsb.freeDeviceList(list, true);

            if (device != null) {
                log.info("valid device found");

            }
        }


    }

    @PreDestroy
    public void close() {
        log.info("LibUsb exiting...");
        LibUsb.exit(context);
    }
}
