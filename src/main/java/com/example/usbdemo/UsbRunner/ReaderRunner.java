package com.example.usbdemo.UsbRunner;

import com.example.usbdemo.initializer.PiGioConfig;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fazecast.jSerialComm.SerialPort;

@Component
@Slf4j
public class ReaderRunner {


    private boolean toggle = false;


    private final PiGioConfig piGioConfig;

    public ReaderRunner(PiGioConfig piGioConfig) {
        this.piGioConfig = piGioConfig;
    }


    @Bean
    public void readSerialPort() {

        log.info("Get Ports:");

        SerialPort[] ports = SerialPort.getCommPorts();

        int idx = 0;
        int portNum = 0;
        for (SerialPort port : ports) {
            log.info(port.getSystemPortName()+" - "+port.getDescriptivePortName());
            if (port.getSystemPortName().equals("ttyACM0")) {
                portNum = idx;
            }
            idx++;
        }

        SerialPort comPort = SerialPort.getCommPorts()[portNum];
        comPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(SerialPortEvent event) {
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;
                byte[] newData = new byte[comPort.bytesAvailable()];
                int numRead = comPort.readBytes(newData, newData.length);
                String message = new String(newData, 0, numRead);
               log.info("Read " + numRead + " bytes : "+message);
                try {
                    toggleLed();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        });

        comPort.openPort();


    }

    private void toggleLed() throws InterruptedException {
        toggle =!toggle;
        final DigitalOutput fanOut = toggle ? piGioConfig.getDoorwayIn() : piGioConfig.getDoorwayOut();

        log.info("activate FanOut: " +fanOut.getName());
        fanOut.state(DigitalState.HIGH);
        Thread.sleep(500);
        fanOut.state(DigitalState.LOW);

    }

}