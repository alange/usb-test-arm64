package com.example.usbdemo.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

@Slf4j
public class AuthenticationService {

    private static final String AUTH_TOKEN_HEADER_NAME = "X-API-KEY";
    private static final String AUTH_TOKEN = "sk-5SjilrKqt25ylrF2cGxcdEAoGX0UbOjSyZqg7XJgWQFlTmqE";

    public static Authentication getAuthentication(HttpServletRequest request) {
        String apiKey = request.getHeader(AUTH_TOKEN_HEADER_NAME);
        if (apiKey == null || !apiKey.equals(AUTH_TOKEN)) {
            String message = String.format("Invalid API Key: %s from %s", apiKey,request.getRemoteAddr());
            throw new BadCredentialsException(message);
        }

        log.info("Authentication successful: {} from {}", apiKey,request.getRemoteAddr());
        return new ApiKeyAuthentication(apiKey, AuthorityUtils.NO_AUTHORITIES);
    }
}
